import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.time.Duration

plugins {
    id("org.springframework.boot") version "2.2.1.RELEASE"
    id("io.spring.dependency-management") version "1.0.9.RELEASE"
    id("com.avast.gradle.docker-compose") version "0.10.9"
    kotlin("jvm") version "1.3.61"
    kotlin("plugin.spring") version "1.3.61"
}

group = "net.gowri"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

val developmentOnly by configurations.creating
configurations {
    runtimeClasspath {
        extendsFrom(developmentOnly)
    }
}

repositories {
    mavenCentral()
    maven { url = uri("https://repo.spring.io/milestone") }
    maven { url = uri("https://packages.confluent.io/maven/") }
    jcenter()
}

buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath("com.commercehub.gradle.plugin:gradle-avro-plugin:0.17.0")
    }
}

apply(plugin = "com.commercehub.gradle.plugin.avro")

configurations.all {
    exclude("org.slf4j", "slf4j-log4j12")
}

dockerCompose{
    //    dockerComposeWorkingDirectory = "${buildDir}/resources/main"
    useComposeFiles = listOf("src/main/docker/docker-compose.yml")
    dockerComposeStopTimeout = Duration.ofSeconds(120)

}

dependencies {

    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.jetbrains.kotlin:kotlin-reflect")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("org.springframework.kafka:spring-kafka")
    implementation("com.ibm.mq:mq-jms-spring-boot-starter:2.1.2")
//    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")

    compile("io.confluent:common-config:5.2.1")
    compile("io.confluent:kafka-avro-serializer:5.2.1")
    compile("io.confluent:kafka-schema-registry-client:5.2.1")
    compile("org.apache.avro:avro:1.9.1")
    compile("io.github.microutils:kotlin-logging:1.7.6")



    developmentOnly("org.springframework.boot:spring-boot-devtools")
    testImplementation("org.springframework.boot:spring-boot-starter-test") {
        exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
    }
    testImplementation("org.springframework.kafka:spring-kafka-test")
}

sourceSets {
    main {
        java.srcDir("${buildDir}/generated/java")
    }
}

tasks.withType<Test> {
    useJUnitPlatform()
}

tasks.register<com.commercehub.gradle.plugin.avro.GenerateAvroJavaTask>("generateAvro") {
    source = fileTree("src/main/resources/avsc")
    setOutputDir(file("${buildDir}/generated/java"))
}

tasks.register<Exec>("kafkaUp") {
    group = "kafka"
    commandLine("confluent", "local", "start")
//    parseCommandLineArguments(kotlin.collections.listOf("/kfup"),)
}

tasks.register<Exec>("kafkaDown") {
    group = "kafka"
    commandLine("confluent", "local", "stop")
//    parseCommandLineArguments(kotlin.collections.listOf("/kfup"),)
}


tasks.withType<KotlinCompile> {
    kotlinOptions {
        freeCompilerArgs = listOf("-Xjsr305=strict")
        jvmTarget = "1.8"

    }
    source(tasks["generateAvro"].outputs)
}
