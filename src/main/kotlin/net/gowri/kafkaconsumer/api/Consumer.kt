package net.gowri.kafkaconsumer.api

import io.confluent.gowri.basicavro.Payment
import mu.KotlinLogging
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.springframework.jms.JmsException
import org.springframework.jms.core.JmsTemplate
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.stereotype.Component

@Component
class Consumer(val jmsTemplate: JmsTemplate) {

    companion object{
        private const val destinationName = "DEV.QUEUE.1"
    }

    private val log = KotlinLogging.logger {}

    @KafkaListener(topics = ["testtopic"], groupId = "cg1")
    fun kafkaSubsriber(message : ConsumerRecord<String, Payment >){
        log.info { "Recieved message from partition : ${message.value()}"  }
        send(message.value())
    }

    fun send(payment: Payment) : String{
        try {
            val messageCreator = JmsMessageCreator(payment.toString().toByteArray())
            jmsTemplate.convertAndSend(destinationName, messageCreator)
        } catch (ex : JmsException){
            log.error(ex) { "Error sending message to MQ = {}"   }
        }

        log.info("successfully possted message to MQ" )
        return "OK"
    }

}