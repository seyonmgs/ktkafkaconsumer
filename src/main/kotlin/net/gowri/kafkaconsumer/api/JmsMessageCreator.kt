package net.gowri.kafkaconsumer.api

import org.springframework.jms.core.MessageCreator
import javax.jms.Message
import javax.jms.Session

class JmsMessageCreator(private val byteArray: ByteArray) : MessageCreator {
    override fun createMessage(session: Session): Message {
        val bytesMessage = session.createBytesMessage()
        bytesMessage.writeBytes(byteArray)
        return bytesMessage
    }
}