package net.gowri.kafkaconsumer.annotation

import net.gowri.kafkaconsumer.KafkaconsumerApplication
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit.jupiter.SpringExtension

@Target(AnnotationTarget.CLASS)
@Retention
@ExtendWith(SpringExtension::class)
@ActiveProfiles("test")
@SpringBootTest(classes = [KafkaconsumerApplication::class])
annotation class TestMeta {
}